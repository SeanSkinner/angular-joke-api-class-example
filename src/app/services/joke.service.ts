import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Joke } from '../models/Joke';
import { map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class JokeService {

  constructor(private readonly http: HttpClient) { }

  private readonly _jokes$: BehaviorSubject<Joke[]> = new BehaviorSubject<Joke[]>([])

  public fetchJokes(): void {
    this.http.get<JokeResponse>("https://v2.jokeapi.dev/joke/Programming?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&type=twopart&amount=10")
    .pipe(
      map((jokeResponse: JokeResponse) => {
        return jokeResponse.jokes
      })
    )
    .subscribe({
      next: (jokes: Joke[]) => {
        console.log("jokes from response",jokes)
        this._jokes$.next(jokes)
      },
      error: (error: HttpErrorResponse) => {
        console.log(error.message)
      }
    })    
  }

  public addJoke(joke: Joke): void {
    this._jokes$.next([...this._jokes$.value, joke])
  }

  public get jokes$(): Observable<Joke[]> {
    return this._jokes$.asObservable()
  }
}

interface JokeResponse {
  jokes: Joke[]
}
