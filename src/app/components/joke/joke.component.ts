import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Joke } from 'src/app/models/Joke';
import { JokeService } from 'src/app/services/joke.service';

@Component({
  selector: 'app-joke',
  templateUrl: './joke.component.html',
  styleUrls: ['./joke.component.css']
})
export class JokeComponent implements OnInit {
  constructor(private readonly jokeService: JokeService) {    
  }
  ngOnInit(): void {
    this.jokeService.fetchJokes()
  }

  public get jokes$(): Observable<Joke[]> {
    return this.jokeService.jokes$
  }

  public handleAddJokeClicked(form: NgForm) {
    const joke: Joke = {
      id: 999,
      setup: form.value.setup,
      delivery: form.value.delivery
    }
    this.jokeService.addJoke(joke)
  }
}
